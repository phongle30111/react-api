package react.api.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Jul 31, 2023 9:48:20 AM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : PhongLT16
 */
@Entity(name = "Account")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;

	@Column(columnDefinition = "varchar(100)")
	private String userAccount;

	@Column(columnDefinition = "nvarchar(200)")
	private String name;

	@Column(columnDefinition = "varchar(100)")
	private String password;

	@Column(columnDefinition = "varchar(100)")
	private String email;

	@Column(columnDefinition = "varchar(11)")
	private String phone;

	@Column(columnDefinition = "nvarchar(200)")
	private String address;

	@ManyToOne
	@JoinColumn(name = "roleId")
	private Role role;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonBackReference
	private List<Cart> carts;

}
