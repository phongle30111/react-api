package react.api.entities.DTO;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import react.api.entities.Category;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 4, 2023 4:37:27 PM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
@Data
public class MedicineDTO {

	private Long medicineId;

	private String medicineName;

	private String mainIngredients;

	private String uses;

	private Long price;

	private String unit;

	private Long quantity;

	private MultipartFile img;

	private Category categoryID;
}
