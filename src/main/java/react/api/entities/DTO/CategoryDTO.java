package react.api.entities.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 4, 2023 4:22:57 PM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDTO {

	private Long categoryId;

	private String categoryName;

}
