package react.api.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Jul 31, 2023 1:58:44 PM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Medicine {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long medicineId;

	@Column(columnDefinition = "nvarchar(100)")
	private String medicineName;

	@Column(columnDefinition = "nvarchar(100)")
	private String mainIngredients;

	@Column(columnDefinition = "nvarchar(2000)")
	private String uses;

	@Column(columnDefinition = "bigint")
	private Long price;

	private String unit;

	@Column(columnDefinition = "bigint")
	private Long quantity;

	private String img;

	@ManyToOne
	@JoinColumn(name = "categoryId")
	private Category category;

	@OneToMany(mappedBy = "medicine", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonBackReference
	private List<Cart> carts;

}
