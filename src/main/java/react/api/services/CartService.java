package react.api.services;

import java.util.List;

import react.api.entities.Cart;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 11:28:58 PM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
public interface CartService {

	void add(Cart cart);

	void update(Cart cart);

	List<Cart> findAllByCartId(Long id);

	void deleteById(Long id);

}
