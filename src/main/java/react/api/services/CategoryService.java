package react.api.services;

import java.util.List;

import react.api.entities.Category;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 4, 2023 10:08:06 AM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
public interface CategoryService {

	List<Category> findAll();

	Category findById(Long id);
}
