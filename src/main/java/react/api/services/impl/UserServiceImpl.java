package react.api.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import react.api.entities.User;
import react.api.repositories.UserRepository;
import react.api.services.UserService;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 1:45:41 PM
 * @System_Name : React
 * @Version : 1.0
 * @Create_by : @author
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository;

	@Override
	public void create(User user) {
		repository.save(user);
	}

	@Override
	public void update(User user) {
		repository.save(user);
	}

	@Override
	public User getUserById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);

	}

	@Override
	public User findByUserAccount(String account) {
		return repository.findByUserAccount(account);
	}

}
