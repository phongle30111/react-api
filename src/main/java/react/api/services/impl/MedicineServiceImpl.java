package react.api.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import react.api.entities.Medicine;
import react.api.repositories.MedicineRepository;
import react.api.services.MedicineService;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 1:54:38 PM
 * @System_Name : React
 * @Version : 1.0
 * @Create_by : @author
 */
@Service
public class MedicineServiceImpl implements MedicineService {

	@Autowired
	private MedicineRepository repository;

	@Override
	public void save(Medicine medicine) {
		repository.save(medicine);
	}

	@Override
	public void update(Medicine medicine) {
		repository.save(medicine);
	}

	@Override
	public Medicine getMedicineById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public List<Medicine> getList() {
		return (List<Medicine>) repository.findAll();
	}

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);

	}

}
