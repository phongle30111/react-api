package react.api.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import react.api.entities.Cart;
import react.api.repositories.CartRepository;
import react.api.services.CartService;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 11:28:58 PM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
@Service
public class CartServiceImpl implements CartService {
	@Autowired
	private CartRepository repository;

	@Override
	public void add(Cart cart) {
		repository.save(cart);

	}

	@Override
	public void update(Cart cart) {
		repository.save(cart);
	}

	@Override
	public List<Cart> findAllByCartId(Long id) {
		return (List<Cart>) repository.findAllByCartId(id);
	}

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);

	}

}
