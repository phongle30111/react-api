package react.api.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import react.api.entities.Category;
import react.api.repositories.CategoryRepository;
import react.api.services.CategoryService;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 11:28:58 PM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository repository;

	@Override
	public List<Category> findAll() {
		return (List<Category>) repository.findAll();
	}

	@Override
	public Category findById(Long id) {
		return repository.findById(id).orElse(null);
	}

}
