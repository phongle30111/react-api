package react.api.services;

import react.api.entities.User;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 1:45:41 PM
 * @System_Name : React
 * @Version : 1.0
 * @Create_by : @author
 */

public interface UserService {

	void create(User user);

	void update(User user);

	User getUserById(Long id);

	void deleteById(Long id);

	User findByUserAccount(String account);

}
