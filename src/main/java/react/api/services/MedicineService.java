package react.api.services;

import java.util.List;

import react.api.entities.Medicine;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 1:54:38 PM
 * @System_Name : React
 * @Version : 1.0
 * @Create_by : @author
 */
public interface MedicineService {

	void save(Medicine medicine);

	void update(Medicine medicine);

	Medicine getMedicineById(Long id);

	List<Medicine> getList();

	void deleteById(Long id);

}
