package react.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

/**
 * @author		: PhongLT16
 * @from 		: DN23_FR_JAVA_04
 * @Created_date: Aug 4, 2023  10:58:02 PM
 * @System_Name	: react
 * @Version		: 1.0
 * @Create_by	: @author
 */
@Configuration
public class MyConfig {
    @Bean
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }
}
