package react.api.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import react.api.entities.Medicine;
import react.api.entities.DTO.MedicineDTO;
import react.api.repositories.CategoryRepository;
import react.api.services.impl.CategoryServiceImpl;
import react.api.services.impl.MedicineServiceImpl;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 1:53:16 PM
 * @System_Name : React
 * @Version : 1.0
 * @Create_by : @author
 */
@RestController
@RequestMapping("/product")
@CrossOrigin
public class MedicineController {

	@Autowired
	MedicineServiceImpl medicineService;

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	CategoryServiceImpl categoryServiceImpl;

	@GetMapping("/list")
	public ResponseEntity<List<Medicine>> showMedicines() {
		List<Medicine> medicines = medicineService.getList();
		return new ResponseEntity<List<Medicine>>(medicines, HttpStatus.OK);
	}

	@GetMapping("/detail/{id}")
	public ResponseEntity<Medicine> getMedicineById(@PathVariable Long id) {
		Medicine medicine = medicineService.getMedicineById(id);
		return ResponseEntity.ok(medicine);
	}

	@PostMapping("/save")
	public ResponseEntity<String> saveMedicine(@ModelAttribute MedicineDTO medicine
//			, HttpServletRequest servletRequest
	) throws IOException {
		System.err.println(medicine.getImg());
		Medicine newMedicine = new Medicine();
		newMedicine.setMedicineName(medicine.getMedicineName());
		newMedicine.setMainIngredients(medicine.getMainIngredients());
		newMedicine.setUses(medicine.getUses());
		newMedicine.setQuantity(medicine.getQuantity());
		newMedicine.setUnit(medicine.getUnit());
		newMedicine.setPrice(medicine.getPrice());
		newMedicine.setCategory(medicine.getCategoryID());

		if (!medicine.getImg().isEmpty()) {
			byte[] bytes = medicine.getImg().getBytes();
			Path path = Paths.get("src/main/resources/static/image/" + medicine.getImg().getOriginalFilename());
			Files.write(path, bytes);
			newMedicine.setImg(medicine.getImg().getOriginalFilename());
		}

//		String fileName = Math.random() + medicine.getImg().getOriginalFilename();
//		File file = new File(servletRequest.getServletContext().getRealPath("src/main/resources/static/image/"),
//				fileName);
//		try {
//			medicine.getImg().transferTo(file);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		newMedicine.setImg(fileName);

		medicineService.save(newMedicine);
		return ResponseEntity.ok().body("success");
	}
}