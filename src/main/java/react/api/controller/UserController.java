package react.api.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import react.api.entities.Role;
import react.api.entities.User;
import react.api.services.impl.UserServiceImpl;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 11:25:16 PM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

	@Autowired
	UserServiceImpl userService;

	@PostMapping("/register")
	public ResponseEntity<Map<String, Object>> registerAccount(@RequestBody User user) {
		Map<String, Object> map = new HashMap<>();
		User userCheckAccount = userService.findByUserAccount(user.getUserAccount());
		if (userCheckAccount != null) {
			map.put("Insert-Fail", user);
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(map);
		}
		Role roleCustomer = new Role((long) 2);
		user.setRole(roleCustomer);
		userService.create(user);
		map.put("Message", "Insert Success");
		return ResponseEntity.ok().body(map);
	}

	@PostMapping("/login")
	public ResponseEntity<?> checkLogin(@RequestBody User user) {
		User checkUser = userService.findByUserAccount(user.getUserAccount());

		if (checkUser == null || !(checkUser.getPassword().equals(user.getPassword()))) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(user);
		}
		return ResponseEntity.ok().body(user.getRole());
	}
}
