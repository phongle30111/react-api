package react.api.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author		: PhongLT16
 * @from 		: DN23_FR_JAVA_04
 * @Created_date: Aug 2, 2023  4:14:05 PM
 * @System_Name	: react
 * @Version		: 1.0
 * @Create_by	: @author
 */
@RestController
@RequestMapping("/cart")
@CrossOrigin
public class CartController {

}
