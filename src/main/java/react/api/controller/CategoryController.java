package react.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import react.api.entities.Category;
import react.api.services.impl.CategoryServiceImpl;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 4, 2023 10:16:09 AM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
@RestController
@RequestMapping("/category")
@CrossOrigin
public class CategoryController {

	@Autowired
	private CategoryServiceImpl service;

	@GetMapping("/list")
	public ResponseEntity<List<Category>> getCategorys() {
		List<Category> categorys = service.findAll();
		return new ResponseEntity<List<Category>>(categorys, HttpStatus.OK);
	}
}
