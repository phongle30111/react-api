package react.api.repositories;

import org.springframework.data.repository.CrudRepository;

import react.api.entities.Medicine;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 1:51:04 PM
 * @System_Name : React
 * @Version : 1.0
 * @Create_by : @author
 */
public interface MedicineRepository extends CrudRepository<Medicine, Long> {

}
