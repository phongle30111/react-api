package react.api.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import react.api.entities.Cart;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 11:27:11 PM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
public interface CartRepository extends CrudRepository<Cart, Long> {

	List<Cart> findAllByCartId(Long cartId);
}
