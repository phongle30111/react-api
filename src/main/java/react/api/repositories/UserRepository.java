package react.api.repositories;

import org.springframework.data.repository.CrudRepository;

import react.api.entities.User;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 1, 2023 10:30:36 AM
 * @System_Name : React
 * @Version : 1.0
 * @Create_by : @author
 */
public interface UserRepository extends CrudRepository<User, Long> {

	User findByUserAccount(String userAccount);
}
