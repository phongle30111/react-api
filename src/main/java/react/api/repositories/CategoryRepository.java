package react.api.repositories;

import org.springframework.data.repository.CrudRepository;

import react.api.entities.Category;

/**
 * @author : PhongLT16
 * @from : DN23_FR_JAVA_04
 * @Created_date: Aug 4, 2023 10:04:15 AM
 * @System_Name : react
 * @Version : 1.0
 * @Create_by : @author
 */
public interface CategoryRepository extends CrudRepository<Category, Long> {

}
